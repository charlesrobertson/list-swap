import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { data } from './app.data';

// DO NOT IMPORT THE TWEENMAX MODULE
// ADD TWEENMAX VARIABLES INSTEAD, USING 'DECLARE'

declare var ease, TweenMax, Back, Bounce, Elastic, Expo, Linear, Power2: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  users = data;

  height: number = 56;
  margin: number = 0;
  duration1: number = 0.5;
  duration2: number = this.duration1 + 0.1;
  ease: any = Elastic.easeOut.config(1, 0.3);

  listSortAnimationForm: FormGroup;
  listSortAnimation: FormControl;

  constructor() {}

  ngOnInit() {
    // set up form control and listen to changes
    this.listSortAnimation = new FormControl(null);
    this.listSortAnimationForm = new FormGroup({
      listSortAnimation: this.listSortAnimation
    });
    this.listSortAnimation.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged()
    )
    .subscribe( (item) => {
      this.tweenProperties(item);
    });
    // set initial select list value
    this.listSortAnimationForm.controls['listSortAnimation'].setValue('elastic ease out');
  }

  // PUBLIC METHODS

  moveListItem(id1: number, id2: number, direction: string = 'up'): void {
    let ids = [1, 1];
    const maxIndex = this.users.length - 1;
    // check to see if the item is at the start or at the end of the list
    ids = direction === 'up' && id1 === 0 || direction === 'down' && id1 === maxIndex ? [1, 0] : [1, 1] ;
    // adjust the target id value, if the current id is at the start or at the end of the list
    id2 = direction === 'up' && id1 === 0 ? maxIndex : (direction === 'down' && id1 === maxIndex ? 0 : id2); 
    const moveOthers = ids[1] === 0 ? true : false;
    const current = document.querySelector('.user-' + id1 + '');
    const target = document.querySelector('.user-' + id2 + '');
    if (current && target) {
      const cTop = getComputedStyle(current).top;
      const tTop = getComputedStyle(target).top;
      // move the current element
      if (ids[0] === 1) {
        this.tweenCurrent(current,tTop,cTop,id1,id2,ids);
      }
      // move the target element
      if (ids[1] === 1) {
        this.tweenTarget(target,cTop);
      }
      // move other elements in the list, if the current element is at the start or at the end of the list
      if (moveOthers) {
        this.users.filter( (user) => {
          return user['userid'] !== id1;
        })
        .map( (user, index) => {
          const height = this.height + this.margin;
          const top = direction === 'up' ? user['position'] - height : user['position'] + height;
          this.tweenOthers(user,top);
        });
      }
    }
  }

  // PRIVATE METHODS

  private tweenCurrent(current: any, tTop: string, cTop: string, id1: number, id2: number, ids: any): void {
    TweenMax.to(current, this.duration2, {
      top: tTop,
      ease: this.ease,
      onComplete: function() {
        // update the position of the item in the array to match that of its layout position
        this.updatePosition(id1,id2,ids,tTop,cTop);
        // now reorder the array by position
        this.reOrderUsers();
        // now update every item's 'userid' to match its position index
        this.reIdUsers();
      }.bind(this)
    });
  }

  private tweenTarget(target: any, cTop: string): void {
    TweenMax.to(target, this.duration1, {
      top: cTop,
      ease: this.ease
    });
  }

  private tweenOthers(user: any, top: number): void {
    TweenMax.to('li#user-' + user['userid'], this.duration1, {
      top: top + 'px',
      onComplete: function() {
        user['position'] = top > 0 ? top : 0;
      }
    });
  }

  private tweenProperties(item: string): void {
    // please go to GreenSock's website for further information on TweenMax's easing capabilities:
    // https://greensock.com/ease-visualizer
    switch(item) {
      case 'back ease out':
        this.duration1 = 0.5;
        this.duration2 = this.duration1 + 0.1;
        this.ease = Back.easeOut.config(1.7);
        break;
      case 'elastic ease out':
        this.duration1 = 0.5;
        this.duration2 = this.duration1 + 0.1;
        this.ease = Elastic.easeOut.config(1, 0.3);
        break;  
      case 'expo ease out':
        this.duration1 = 0.5;
        this.duration2 = this.duration1 + 0.1;
        this.ease = Expo.easeOut;
        break;  
      case 'linear ease none':
        this.duration1 = 0.1;
        this.duration2 = this.duration1 + 0.1;
        this.ease = Linear.easeNone;
        break;
      default:
        this.duration1 = 0.5;
        this.duration2 = this.duration1 + 0.1;
        this.ease = Elastic.easeOut.config(1, 0.3);
    }
  }

  private reOrderUsers(): void {
    this.users.sort(function(a, b) {
      return a.position - b.position;
    });
  }

  private reIdUsers(): void {
    this.users.map( (user, index) => {
      user['userid'] = index;
    });
  }

  private updatePosition(id1: number, id2: number, ids: any, top1: string, top2: string): void {
    this.users.map( (user) => {
      user['position'] = user['userid'] === id1 && ids[0] === 1 ? parseInt(top1) : (user['userid'] === id2 && ids[1] === 1 ? parseInt(top2) : user['position']);
    });
  }

}

